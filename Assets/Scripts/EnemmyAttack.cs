using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemmyAttack : MonoBehaviour
{
    [SerializeField] private float damage = 40f;
    [SerializeField] private EnemyAnimation enemyAnimation;
    private Transform target;

    private void Start()
    {
        target = TargetsForEnemy.Instance.GetTarget();
    }

    private void OnEnable()
    {
        enemyAnimation.AnimationAttackMoment += AttackTarget;
    }

    private void OnDisable()
    {
        enemyAnimation.AnimationAttackMoment -= AttackTarget;
    }

    public void AttackTarget()
    {
        target.GetComponent<PlayerHealth>().TakeDamage(damage);
    }
}
