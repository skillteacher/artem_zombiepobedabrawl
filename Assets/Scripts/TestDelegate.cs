using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDelegate : MonoBehaviour
{
    public delegate void WriteDelegate();
    public WriteDelegate writeToLog;
    public event WriteDelegate WriteEvent;

    private void Write()
    {
        Debug.Log("Write");
    }
    private void Write2()
    {
        Debug.Log("2");
    }
    private void Awake()
    {
        writeToLog = Write;
        writeToLog += Write2;
        writeToLog += Write;
    }
    private void Start()
    {
        //writeToLog();
        WriteEvent?.Invoke();
    }
    private void OnEnable()
    {
        WriteEvent += Write;    
    }    
        
    private void OnDisable()
    {
    WriteEvent -= Write;
    }
}